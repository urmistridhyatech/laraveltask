<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class secure_data
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $data=$request->all();
         
         $data['t2'] = Hash::make($request->t2);
      $request->route()->setParameter('t1',Hash::make($request->t1));
          // Input modification
//            $request->replace($data);

    
        return $next($request);
    
    }
}

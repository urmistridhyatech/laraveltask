<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\post;
use Auth;
use Illuminate\Support\Facades\Hash;
use Image;

class Post_c extends Controller
{
    function post_f()
    {
    return view("post",array("msg"=>""));
    }


    function post_ins(Request $r)
    {
	
	 $vd = $r->validate([
        'img' => 'required|image',
        'dis'=>'required'
    ]);


    	$dis=$r->dis;
    	$img = time().'.'.$r->img->getClientOriginalExtension();
    	$r->img->move(public_path('img'),$img);

	$p=new post;
	$p->uid=Auth::User()->id;
	$p->img=$img;
	$p->data=$dis;
	$p->save();

		return view("post",array("msg"=>"Success..."));
    }


    public function display(Request $Request)
    {
            print_r( $Request->all());
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">


        <div class="col-md-8">
            <div class="card">
<div class="text-center">
    <b class="text-success"> {{ $msg ?? "" }}</b>
</div>
                <div class="card-header bg-success text-white "><b>Post</b></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('post_ins') }}" enctype="multipart/form-data">
                       
                        @csrf


                       

                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Image : </label>

                            <div class="col-md-6">
                                <input id="name" type="file" class="form-control @error('img') is-invalid @enderror" name="img" value="{{ old('img') }}" required autocomplete="img" autofocus style="padding: 2px;">

                                @error('img')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                            <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Discription : </label>

                            <div class="col-md-6">

<textarea class="form-control @error('dis') is-invalid @enderror" name="dis" value="{{ old('dis') }}" required autocomplete="name" autofocus rows="4" >
    
</textarea>

                                @error('dis')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
